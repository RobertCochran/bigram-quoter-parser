/*
 * bigram-quoter-parser - sample parser for the save format of
 * Joshua Brockschmidt's bigram_quoter
 *
 * Copyright (C) 2015 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

%{
	#include <stdio.h>
	#include <stdlib.h>
	extern int yylex();
	extern FILE *yyin;
	extern unsigned int current_line;
	void yyerror(const char *s);
	int yydebug = 1;
%}

%token SAVE_VERSION_MAJ
%token SAVE_VERSION_MIN
%token BIGRAM_WORDS
%token BIGRAM_ARRAY
%token MARKER_START MARKER_PERIOD MARKER_EXCLAIM MARKER_QUESTION

%union {
	unsigned int number;
	char *string;
}

%token <string> STRING
%token <number> NUMBER

%%

bigram_save_format: save_version words_assn array_assn ;

save_version
	: save_version_maj_assn save_version_min_assn
	| save_version_min_assn save_version_maj_assn
	;

save_version_maj_assn
	: SAVE_VERSION_MAJ '=' NUMBER
	;

save_version_min_assn
	: SAVE_VERSION_MIN '=' NUMBER
	;

words_assn
	: BIGRAM_WORDS '=' array
	;

array_assn
	: BIGRAM_ARRAY '=' array
	;

array
	: '[' array_exp ']'
	| '[' array_exp ',' ']'
	| '[' ']'
	;

array_exp
	: array_element
	| array_exp ',' array_element
	| array
	| array_exp ',' array
	;

array_element: NUMBER | STRING | marker;

marker: MARKER_START | MARKER_PERIOD | MARKER_EXCLAIM | MARKER_QUESTION ;

%%

#include <stdio.h>

int main (int argc, char **argv)
{
	if (argc < 2) {
		printf("Need file argument\n");
		return -1;
	}

	FILE *input;

	if ((input = fopen(argv[1], "r")) == NULL) {
		perror("Couldn't open file");
		return -1;
        }

	yyin = input;

	while(yyparse() != 0);
}

void yyerror (const char *s)
{
	printf("Oops! %s on line %d\n", s, current_line);
	exit(-1);
}
