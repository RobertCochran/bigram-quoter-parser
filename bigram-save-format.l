/*
 * bigram-quoter-parser - sample parser for the save format of
 * Joshua Brockschmidt's bigram_quoter
 *
 * Copyright (C) 2015 Robert Cochran
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License in the LICENSE file for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

%{
	#include "bigram-save-format.tab.h"
	unsigned int current_line = 1;
%}

%option noyywrap

%%

\n {current_line++;}
bigram_save_version_major {return SAVE_VERSION_MAJ;}
bigram_save_version_minor {return SAVE_VERSION_MIN;}
bigram_words {return BIGRAM_WORDS;}
bigram_array {return BIGRAM_ARRAY;}

marker_start {return MARKER_START;}
marker_period {return MARKER_PERIOD;}
marker_exclaim {return MARKER_EXCLAIM;}
marker_question {return MARKER_QUESTION;}

\"(\\.|[^"])*\" {yylval.string = '\0'; return STRING;}
[0-9]+ {yylval.number = strtol(yytext, NULL, 10); return NUMBER;}
= {return '=';}
\[ {return '[';}
\] {return ']';}
, {return ',';}
[ \t] {/* Do nothing */}

%%

