#
# bigram-quoter-parser - sample parser for the save format of
# Joshua Brockschmidt's bigram_quoter
#
# Copyright (C) 2015 Robert Cochran
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License in the LICENSE file for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

all: parser

bigram-save-format.tab.c bigram-save-format.tab.h: bigram-save-format.y
	bison -d bigram-save-format.y

lex.yy.c: bigram-save-format.l bigram-save-format.tab.h
	flex bigram-save-format.l

parser: lex.yy.c bigram-save-format.tab.c bigram-save-format.tab.h
	gcc lex.yy.c bigram-save-format.tab.c -o parser

clean:
	rm bigram-save-format.tab.h bigram-save-format.tab.c lex.yy.c parser
